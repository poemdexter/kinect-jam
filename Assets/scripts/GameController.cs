﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public UILabel p1Text, p2Text, p1HPText, p2HPText, title, winner;
    private int p1Score, p2Score;
    public Shoot p1Shoot, p2Shoot;
    public Movement p1Move, p2Move;
    public GameObject instructions1, instructions2, instructions3;
    private bool ready;


    public void Score(string playerTag)
    {
        if (ready)
        {
            if (playerTag.Equals("Player1"))
            {
                p2Score++;
                p2Text.text = "KILLS:" + p2Score;
            }

            if (playerTag.Equals("Player2"))
            {
                p1Score++;
                p1Text.text = "KILLS:" + p1Score;
            }

            if (p1Score == 10)
            {
                Win(1);
            }
            if (p2Score == 10)
            {
                Win(2);
            }
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Ready();
        }

        if (Input.GetButtonDown("Start_1") || Input.GetButtonDown("Start_2"))
        {
            UnReady();
        }
    }

    public void Ready()
    {
        if (!ready)
        {

            p1HPText.enabled = true;
            p2HPText.enabled = true;
            GameObject.FindGameObjectWithTag("Player1").GetComponent<Combat>().Reset();
            GameObject.FindGameObjectWithTag("Player2").GetComponent<Combat>().Reset();
            p1Score = 0;
            p2Score = 0;
            p1Text.enabled = true;
            p2Text.enabled = true;
            p1Text.text = "KILLS:" + p1Score;
            p2Text.text = "KILLS:" + p2Score;
            ready = true;
            winner.enabled = false;
            title.enabled = false;
            instructions1.SetActive(false);
            instructions2.SetActive(false);
            instructions3.SetActive(false);
            p1Shoot.ready = true;
            p2Shoot.ready = true;
            p1Move.ready = true;
            p2Move.ready = true;
        }
    }

    public void UnReady()
    {
        ready = false;
        
        p1Shoot.ready = false;
        p2Shoot.ready = false;
        p1Move.ready = false;
        p2Move.ready = false;
        p1Text.enabled = false;
        p2Text.enabled = false;
        p1HPText.enabled = false;
        p2HPText.enabled = false;
        instructions1.SetActive(true);
        instructions2.SetActive(true); 
        instructions3.SetActive(true);
    }

    private void Win(int team)
    {
        if (team == 1)
            winner.text = "RED TEAM WINS!";
        else if (team == 2)
            winner.text = "BLUE TEAM WINS!";

        winner.enabled = true;
        UnReady();
    }
}