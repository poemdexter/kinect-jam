﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GunKickback gun;
    public Vector3 bulletOffset;
    private bool isShooting;
    private Movement move;
    private float facing = 1f;

    public bool ready;

    private void Start()
    {
        move = GetComponent<Movement>();
    }

    private void Update()
    {
        if (ready)
        {
            if (CompareTag("Player1") && Input.GetButtonDown("X_1"))
            {
                isShooting = true;
            }

            if (CompareTag("Player1") && Input.GetButtonUp("X_1"))
            {
                isShooting = false;
            }

            if (CompareTag("Player2") && Input.GetButtonDown("X_2"))
            {
                isShooting = true;
            }

            if (CompareTag("Player2") && Input.GetButtonUp("X_2"))
            {
                isShooting = false;
            }

            gun.IsShooting = (isShooting) ? true : false;

            if (move.MoveDirection != 0) facing = move.MoveDirection;
        }
    }

    public void Fire()
    {
        Vector3 offset = new Vector3(bulletOffset.x * facing, bulletOffset.y, 0);
        GameObject go = (GameObject) Instantiate(bulletPrefab, transform.position + offset, Quaternion.identity);
        go.GetComponent<BulletFired>().facing = facing;
        go.transform.localScale = new Vector3(facing, 1f, 1f);
    }
}