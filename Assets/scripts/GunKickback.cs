﻿using UnityEngine;
using System.Collections;

public class GunKickback : MonoBehaviour
{
    public bool IsShooting { get; set; }
    public float leftX = -.175f;
    public float rightX = .175f;
    public float kickDelay = .1f;
    private float currentTime;
    private bool isOnLeft;
    private Shoot shoot;

    public void Start()
    {
        currentTime = kickDelay; // start ready to kick
    }

    public void Update()
    {
        if (IsShooting)
        {
            float pos = (isOnLeft) ? leftX : rightX;
            if ((currentTime += Time.deltaTime) > kickDelay)
            {
                currentTime = 0;
                transform.localPosition = new Vector3(pos, -0.075f, 0);
                if (isOnLeft)
                    GetShoot().Fire();

                isOnLeft = !isOnLeft;
            }
        }
    }

    private Shoot GetShoot()
    {
        if (shoot == null)
            shoot = transform.parent.parent.GetComponent<Shoot>();
        return shoot;
    }
}