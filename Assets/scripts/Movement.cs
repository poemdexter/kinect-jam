﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public float speed;
    private Animator anim;
    public float MoveDirection { get; set; }
    public Parallax parallax;

    public bool ready;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (ready)
        {
            MoveDirection = 0;
            float xMove = 0;

            if (CompareTag("Player1"))
                xMove = Input.GetAxisRaw("L_XAxis_1");

            if (CompareTag("Player2"))
                xMove = Input.GetAxisRaw("L_XAxis_2");

            if (xMove > 0)
            {
                MoveDirection = 1f;
            }
            else if (xMove < 0)
            {
                MoveDirection = -1f;
            }
            else
            {
                MoveDirection = 0;
            }

            if (MoveDirection != 0)
            {
                transform.localScale = new Vector3(MoveDirection, 1f, 1f);
                transform.Translate(Time.deltaTime*speed*MoveDirection*Vector3.right);
                anim.SetBool("Running", true);
            }
            else
            {
                anim.SetBool("Running", false);
            }

            if (CompareTag("Player1") && Input.GetButtonDown("Start_1"))
            {
                transform.position = new Vector3(-7f, -2.5f, 0);
            }
            if (CompareTag("Player2") && Input.GetButtonDown("Start_2"))
            {
                transform.position = new Vector3(7f, -2.5f, 0);
            }

            parallax.UpdatePos(tag, transform.position);
        }
    }
}